package com.example.demo.models;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepo extends CrudRepository<Car, Long> {
    List<Car> findByModel(String model);

    @Query("select c from Car c where c.price <= %?1%")
    List<Car> findByMaxPrice(Integer price);

    @Query("select c from Car c where c.price >= %?1% and c.price <= %?2%")
    List<Car> findByPriceRange(Integer minPrice, Integer maxPrice);

    Page<Car> findAll(Pageable pageable);
}

package com.example.demo.models;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepo extends CrudRepository<User, Long> {
    User findByUserId(String userid);
    User findByToken(String token);
}

package com.example.demo.security;
import com.example.demo.models.User;
import com.example.demo.models.UserRepo;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.ArrayList;
import java.util.Enumeration;

@Component
public class EntryPoint implements HandlerInterceptor  {
    @Autowired
    UserRepo userRepo;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        System.out.println(request.getRequestURI());
//        ArrayList<String> allowedUrl = new ArrayList<>();
//        allowedUrl.add("/user/login");
//        allowedUrl.add("/user/registration");
//
//        Enumeration<String> headerNames = request.getHeaderNames();
//
//        if (request.getHeader("access-control-request-headers") != null) {
//            return true;
//        }

//        if (headerNames != null) {
//            while (headerNames.hasMoreElements()) {
//                String name = headerNames.nextElement();
//                System.out.println(name + " : " + request.getHeader(name));
//            }
//        }

//        if (allowedUrl.contains(request.getRequestURI())) {
//            // no need token
//            System.out.println("allowed");
//            return true;
//        } else {
//            // need token
//            boolean ok = true;
//            String bearer = request.getHeader("authorization");
//
//            if (bearer != null) {
//                String token = bearer.substring(7);
//                User user = userRepo.findByToken(token);
//                if (user != null) {
//                    return true;
//                }
//            }
//
//            System.out.println(bearer);
//            System.out.println("not allowed");
//            response.setContentType("application/json");
//            String jsonData = "{\"msg\": \"Not Allowed\"}";
//            response.getWriter().write(jsonData);
//            return false; // If you return false, the request will be aborted.
//        }
        return true;
    }
}

package com.example.demo.service;

import com.example.demo.dto.request.LoginRequest;
import com.example.demo.dto.request.SignupRequest;
import com.example.demo.dto.response.JwtResponse;
import com.example.demo.models.User;
import com.example.demo.models.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    @Autowired
    UserRepo userRepo;

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    // login
    public JwtResponse signin(LoginRequest request) {
        // authentication.. if success generate token
        System.out.println(request.getUserId() + " " + request.getPassword());
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUserId(), request.getPassword()));
        User user = userRepo.findByUserId(request.getUserId());

        if (user == null) {
            throw new IllegalArgumentException();
        }

        String jwt = jwtService.generateToken(user);
        return JwtResponse.builder().token(jwt).user(user).build();
    }

    // register a new user
    public JwtResponse signup(SignupRequest request) {
        User user = User.builder()
                .name(request.getName())
                .password(passwordEncoder.encode(request.getPassword()))
                .roles(request.getRoles())
                .userId(request.getUserId())
                .build();
        userRepo.save(user);
        String jwt = jwtService.generateToken(user);
        return JwtResponse.builder().token(jwt).build();
    }
}

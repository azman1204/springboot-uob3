package com.example.demo.dto;

import com.example.demo.models.Car;
import com.example.demo.models.Owner;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarOwner {
    private Owner owner;
    private Car car;

    public CarOwner(Owner owner, Car car) {
        this.owner = owner;
        this.car = car;
    }

    public CarOwner() {
    }
}

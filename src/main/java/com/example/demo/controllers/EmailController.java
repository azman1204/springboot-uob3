package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {
    @Autowired
    private JavaMailSender emailSender;
    @GetMapping("/auth/send-mail")
    public ResponseEntity<?> sendMail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@baeldung.com");
        message.setTo("azman1204@yahoo.com");
        message.setSubject("Hello World");
        message.setText("Hello world content");
        emailSender.send(message);
        return ResponseEntity.ok("mail sent");
    }
}

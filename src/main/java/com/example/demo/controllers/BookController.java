package com.example.demo.controllers;

import com.example.demo.models.Car;
import com.example.demo.models.CarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BookController {
    @Autowired
    CarRepo carRepo;

    @GetMapping("/book-list")
    @ResponseBody
    public String bookList() {
        return "book"; // template/book.html
    }

    @GetMapping("/book-info")
    public String bookInfo(Model model) {
        model.addAttribute("name", "John Doe");
        return "book_info"; // template/book_info.html
    }

    @GetMapping("/car-list")
    public String carList(Model model) {
        Iterable<Car> itr = carRepo.findAll();
        model.addAttribute("cars", itr);
        return "car_list"; // template/car_list.html
    }
}

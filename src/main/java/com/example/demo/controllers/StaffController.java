package com.example.demo.controllers;

import com.example.demo.dto.CarOwner;
import com.example.demo.dto.Resp;
import com.example.demo.models.Car;
import com.example.demo.models.Owner;
import com.example.demo.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class StaffController {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @GetMapping("/staff-listing")
    public List<String> staffListing() {
        //List<String> myList = new ArrayList<>();
        String sql = "SELECT concat(name, '**') AS myname FROM person";
        return jdbcTemplate.query(sql, (rs, row) -> {
            return rs.getString("myname");
        });
    }

    @GetMapping("/staff-listing2")
    public List<Person> staffListing2() {
        String sql = "SELECT * FROM person";
        return jdbcTemplate.query(sql, (rs, row) -> {
            Person p = new Person();
            p.setName(rs.getString("name"));
            p.setAge(rs.getInt("age"));
            p.setSalary(rs.getDouble("salary"));
            return p;
        });
    }

    @GetMapping("/staff-listing3")
    public Resp staffListing3() {
        String sql = "SELECT * FROM car c, OWNER o WHERE c.owner = o.ownerid";

        List<CarOwner> list = jdbcTemplate.query(sql, (rs, row) -> {
            Car c = new Car();
            c.setBrand(rs.getString("brand"));

            Owner o = new Owner();
            o.setFirstname(rs.getString("firstname"));

            CarOwner co = new CarOwner();
            co.setCar(c);
            co.setOwner(o);
            return co;
        });
        return  new Resp(1, list);
    }
}

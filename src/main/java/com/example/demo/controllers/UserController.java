package com.example.demo.controllers;

import com.example.demo.dto.Resp;
import com.example.demo.dto.request.Login;
import com.example.demo.models.User;
import com.example.demo.models.UserRepo;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Random;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    @Autowired
    UserRepo userRepo;

    @PostMapping("/registration")
    public Resp register(@RequestBody User newUser) {
        String salt = BCrypt.gensalt();
        String pwd = BCrypt.hashpw(newUser.getPassword(), salt);
        newUser.setPassword(pwd);
        userRepo.save(newUser);
        return new Resp(200, newUser);
    }

    @PostMapping("/login")
    public Resp login(@RequestBody Login login) {
        System.out.println(login.getUserid() + " " + login.getPassword());
        User user = userRepo.findByUserId(login.getUserid());

        if (BCrypt.checkpw(login.getPassword(), user.getPassword())) {
            String token = generateRandomString(40);
            user.setToken(token);
            userRepo.save(user);
            return new Resp(200, user);
        }

        return new Resp(404, "Wrong Credentials");
    }

    private String generateRandomString(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder randomString = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            randomString.append(characters.charAt(index));
        }

        return randomString.toString();
    }
}

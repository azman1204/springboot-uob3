package com.example.demo.controllers;

import com.example.demo.dto.Resp;
import com.example.demo.models.Upload;
import com.example.demo.models.UploadRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UploadFileController {
    @Autowired
    UploadRepo uploadRepo;

    @Value("${upload.path}")
    String path;
    @PostMapping("/upload")
    public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file ) {
        System.out.println("path = " + path);
        String fileName = file.getOriginalFilename();
        try {
            file.transferTo( new File(path + fileName));
            Upload upload = new Upload();
            upload.setFileName(fileName);
            uploadRepo.save(upload);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(new Resp(200, "Success"));
    }

    @GetMapping("/upload-list")
    public ResponseEntity<?> showAllFiles() {
        Iterable<Upload> uploads = uploadRepo.findAll();
        return ResponseEntity.ok(uploads);
    }

}

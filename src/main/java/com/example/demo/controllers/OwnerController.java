package com.example.demo.controllers;

import com.example.demo.models.Owner;
import com.example.demo.models.OwnerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class OwnerController {
    @Autowired
    OwnerRepo ownerRepo;


    @GetMapping("/owner/{id}")
    public Owner findOne(@PathVariable Long id) {
        Optional<Owner> opt = ownerRepo.findById(id);
        if (opt.isPresent()) {
            return opt.get();
        } else {
            return new Owner();
        }
    }
}

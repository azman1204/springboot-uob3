package com.example.demo.controllers;

import com.example.demo.dto.Resp;
import com.example.demo.models.Car;
import com.example.demo.models.CarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CarController {
    @Autowired
    CarRepo carRepo;

    @GetMapping("/car-search")
    public Resp findAll(@RequestParam Integer page) {
        Pageable paging = PageRequest.of(page - 1, 2);
        Page<Car> cars =  carRepo.findAll(paging);
        Map<String, Object> pagination = new HashMap<>();
        pagination.put("currentPage", cars.getNumber());
        pagination.put("totalItems", cars.getTotalElements());
        pagination.put("totalPages", cars.getTotalPages());
        return new Resp(1, cars.getContent(), pagination);
    }

    @GetMapping("/car/{id}")
    public Resp findOne(@PathVariable Long id) {
        Optional<Car> opt =  carRepo.findById(id);
        if (opt.isPresent())
            return new Resp(1, opt.get());
        else
            return new Resp(404, "id does not exist");
    }

    @PostMapping("/car-save")
    public Resp save(@RequestBody Car carForm) {
        carRepo.save(carForm);
        return new Resp(200, carForm);
    }

    @PutMapping("/car-update/{id}")
    public Resp update(@RequestBody Car newCar, @PathVariable Long id) {
        Optional<Car> opt = carRepo.findById(id);
        if (opt.isPresent()) {
            Car car = opt.get();
            car.setBrand(newCar.getBrand());
            car.setModel(newCar.getModel());
            carRepo.save(car);
            return new Resp(200, car);
        }

        return new Resp(404, "ID does not exist");
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/car-delete/{id}")
    public Resp delete(@PathVariable Long id) {
        carRepo.deleteById(id);
        return new Resp(200, "ok");
    }

    @GetMapping("/car-search-model/{model}")
    public List<Car> findByModel(@PathVariable String model) {
        return carRepo.findByModel(model);
    }

    @GetMapping("/car-search-price/{price}")
    public List<Car> findByPrice(@PathVariable Integer price) {
        return carRepo.findByMaxPrice(price);
    }

    @GetMapping("/car-search-price-range/{minPrice}/{maxPrice}")
    public List<Car> findByPriceRange(@PathVariable Integer minPrice,
                                      @PathVariable Integer maxPrice) {
        return carRepo.findByPriceRange(minPrice, maxPrice);
    }
}

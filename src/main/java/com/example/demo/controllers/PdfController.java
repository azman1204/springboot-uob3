package com.example.demo.controllers;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.WebContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import jakarta.servlet.ServletContext;

@Controller
@RequiredArgsConstructor
public class PdfController {
    @Autowired
    ServletContext servletContext;

    private final TemplateEngine templateEngine;
    @Value("${upload.path}")
    String path;

    @GetMapping("/auth/pdf")
    public ResponseEntity<?> genPdf() throws IOException {
        HtmlConverter.convertToPdf(new File(path + "/sample.html"),new File(path + "sample.pdf"));
        return ResponseEntity.ok("generated..");
    }

    @GetMapping("/auth/pdf2")
    public ResponseEntity<?> genPdf2(Model model) throws IOException {
        Context context = new Context();
        model.addAttribute("name", "Azman Zakaria");
        context.setVariables(model.asMap());
        String html = templateEngine.process("sample2", context);
        ConverterProperties converterProperties = new ConverterProperties();
        converterProperties.setBaseUri("http://localhost:8080");
        ByteArrayOutputStream target = new ByteArrayOutputStream();
        HtmlConverter.convertToPdf(html, target, converterProperties);
        byte[] bytes = target.toByteArray();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=order.pdf")
                .contentType(MediaType.APPLICATION_PDF)
                .body(bytes);
    }
}

package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class Hello {
    @GetMapping("/hello")
    public String hello() {
        // http://localhost:8080/hello
        return "Hello World";
    }

    @GetMapping("/demo")
    public ArrayList<String> demo() {
        ArrayList<String> persons = new ArrayList<>();
        persons.add("John Doe");
        persons.add("Abu Bakar");
        persons.add("Ravi");
        return persons;
    }

    @GetMapping("/demo2")
    public ArrayList<Person> demo2() {
        ArrayList<Person> persons = new ArrayList<>();

        Person p1 = new Person();
        p1.name = "John Doe";
        p1.address = "London";

        Person p2 = new Person();
        p2.name = "Abu Bakar";
        p2.address = "Kuala Pilah";

        persons.add(p1);
        persons.add(p2);
        return persons;
    }
}

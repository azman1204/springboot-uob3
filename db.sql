/*
SQLyog Community
MySQL - 8.2.0 : Database - javatot
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `javatot`;

/*Table structure for table `car` */

CREATE TABLE `car` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand` varchar(50) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `register_number` varchar(20) DEFAULT NULL,
  `year` int DEFAULT NULL,
  `price` int DEFAULT NULL,
  `owner` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `car` */

insert  into `car`(`id`,`brand`,`color`,`model`,`register_number`,`year`,`price`,`owner`) values 
(1,'Toyota','red','Supra','ABC123',2023,100000,1),
(2,'Honda','blue','Civic','DEF123',2022,120000,1),
(3,'Suzuki','gray','Swift','GHI123',2021,125000,2),
(4,'Honda','red','Civic','GHI123',2023,150000,2);

/*Table structure for table `owner` */

CREATE TABLE `owner` (
  `ownerid` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ownerid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `owner` */

insert  into `owner`(`ownerid`,`firstname`,`lastname`) values 
(1,'John','Doe'),
(2,'Tiger','Wood');

/*Table structure for table `person` */

CREATE TABLE `person` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `salary` float(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `person` */

insert  into `person`(`id`,`name`,`age`,`salary`) values 
(1,'Rivaldo',45,1000.00),
(2,'Abu Bakar',42,12000.00),
(3,'Ali',44,13000.00),
(4,'Jane Doe',35,15000.00),
(5,'Jane Doe',35,15000.00),
(6,'John Terry',46,45000.00);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
